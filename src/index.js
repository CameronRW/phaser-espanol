import Phaser from 'phaser';

import backgroundImage from './assets/images/background-city.png';
import buildingImage from './assets/images/building.png';
import carImage from './assets/images/car.png';
import houseImage from './assets/images/house.png';
import treeImage from './assets/images/tree.png';

import treeAudio from './assets/audio/arbol.mp3';
import carAudio from './assets/audio/auto.mp3';
import houseAudio from './assets/audio/casa.mp3';
import buildingAudio from './assets/audio/edificio.mp3';
import correctAudio from './assets/audio/correct.mp3';
import wrongAudio from './assets/audio/wrong.mp3';

// create a new scene named "Game"
const gameScene = new Phaser.Scene('Game');

const config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene,
  title: 'Spanish Learning Game',
  pixelArt: false,
};

// eslint-disable-next-line no-unused-vars
const game = new Phaser.Game(config);

// some parameters for our scene
gameScene.init = function init() {
  // word database
  this.words = [
    {
      key: 'building',
      setXY: {
        x: 100,
        y: 240,
      },
      spanish: 'edificio',
    },
    {
      key: 'house',
      setXY: {
        x: 240,
        y: 280,
      },
      setScale: {
        x: 0.8,
        y: 0.8,
      },
      spanish: 'casa',
    },
    {
      key: 'car',
      setXY: {
        x: 400,
        y: 300,
      },
      setScale: {
        x: 0.8,
        y: 0.8,
      },
      spanish: 'automovil',
    },
    {
      key: 'tree',
      setXY: {
        x: 550,
        y: 250,
      },
      spanish: 'arbol',
    },
  ];
};

// load asset files for our game
gameScene.preload = function preload() {
  // load images
  this.load.image('background', backgroundImage);
  this.load.image('building', buildingImage);
  this.load.image('car', carImage);
  this.load.image('house', houseImage);
  this.load.image('tree', treeImage);

  this.load.audio('treeAudio', treeAudio);
  this.load.audio('carAudio', carAudio);
  this.load.audio('houseAudio', houseAudio);
  this.load.audio('buildingAudio', buildingAudio);
  this.load.audio('correctAudio', correctAudio);
  this.load.audio('wrongAudio', wrongAudio);
};

// executed once, after assets were loaded
gameScene.create = function create() {
  // load background
  this.add.image(0, 0, 'background').setOrigin(0, 0);

  this.items = this.add.group(this.words);

  this.items.setDepth(1);

  const items = this.items.getChildren();

  for (let i = 0; i < items.length; i++) {
    const item = items[i];

    // make the items interactive
    item.setInteractive();

    // creating tween - correct
    item.correctTween = this.tweens.add({
      targets: item,
      scaleX: 1.5,
      scaleY: 1.5,
      duration: 300,
      paused: true,
      yoyo: true,
      ease: 'Quad.easeInOut',
    });

    // creating tween - wrong
    item.wrongTween = this.tweens.add({
      targets: item,
      scaleX: 1.5,
      scaleY: 1.5,
      duration: 300,
      angle: 90,
      paused: true,
      yoyo: true,
      ease: 'Quad.easeInOut',
    });

    // creating tween - transparency
    item.alphaTween = this.tweens.add({
      targets: item,
      alpha: 0.7,
      duration: 200,
      paused: true,
      ease: 'Quad.easeInOut',
    });

    // listen to the pointerdown event
    item.on('pointerdown', () => {
      // console.log(`You clicked ${item.texture.key}`);
      // item.resizeTween.play();
      const result = this.processAnswer(this.words[i].spanish);

      if (result) {
        item.correctTween.play();
      } else {
        item.wrongTween.play();
      }

      // show next querstion
      this.showNextQuestion();
    }, this);

    // listen to the pointerover event
    item.on('pointerover', () => {
      item.alphaTween.play();
    }, this);

    // listen to the pointerout event
    item.on('pointerout', () => {
      // stop alpha tween
      item.alphaTween.stop();

      // set no transparency
      item.alpha = 1;
    });

    // create a sound for each word
    this.words[i].sound = this.sound.add(`${this.words[i].key}Audio`);
  }

  // text object
  this.wordText = this.add.text(30, 20, ' ', {
    font: '28px Open Sans',
    fill: '#fff',
  });

  // correct / wrong sounds
  this.correctSound = this.sound.add('correctAudio');
  this.wrongSound = this.sound.add('wrongAudio');

  // show the first question
  gameScene.showNextQuestion();
};

// Show new question
gameScene.showNextQuestion = function showNextQuestion() {
  // select a random word
  this.nextWord = Phaser.Math.RND.pick(this.words);

  // play a sound for that word
  this.nextWord.sound.play();

  // show the text of the word in Spanish
  this.wordText.setText(this.nextWord.spanish);
};

// Process answer
gameScene.processAnswer = function processAnswer(userResponse) {
  // compare the user response with correct response
  if (userResponse === this.nextWord.spanish) {
    // it's correct
    // play sound
    this.correctSound.play();
    return true;
  } else {
    // it's incorrect
    // play sound
    this.wrongSound.play();
    return false;
  }
};
